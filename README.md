Budgie
=============

Live: http://budgie-midterm.herokuapp.com/

A social employee discount sharing platform. Great for helping you get that discount on something you've been eyeing!

Created as a collaborative midterm project for Lighthouse Labs.